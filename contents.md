Software Development Workflows
==============================

The talk focuses on the optimization of software development workflows for teams,
with the aims of increasing efficiency and improving product quality while 
keeping the fun. In that context it discusses how git, GitHub and other tools
can be used for this purpose.

A laptop with git installed and a GitHub account is recommended for there
will be interactive parts of the presentation. Participation in interactive
parts is optional but recommended.

For any questions before or after the presentation, contact `lasse.schuirmann@gmail.com`.

Agenda
------

- WHOAMI
- Talking to Each Other
- Design
- Review
- Good Commits
- Forking, Branching, Develop, Master?
- Testing
- Documentation
- Internationalization and Localization
- Issue Management
- Releasing
- Advanced Git Tips

About the Speaker
-----------------

Lasse Schuirmann is a freelance software developer who cares passionately about
open source, collaboratively developed software. After he was awarded the Google
Summer of Code sholarship, he began mentoring several students while
administering scholarships for 24 students at GNOME. Lasse is the founder and maintainer
of the coala project which is developed voluntarily by contributors from all over the
world with 5 high quality commits per day on average. Recently he became maintainer 
of the Python Babel project which is used in many industrial and open source projects
for internationalization. Currently Lasse builds a novel software project
management assistant in the aim to increase the efficiently of software development
once and for all.

 * The coala project: http://coala-analyzer.org/
 * The GNOME suite: https://www.gnome.org/
 * The Babel project: http://babel.pocoo.org/